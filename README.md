# Tic-Tac-Toe Lisp Project

This project simulates the game Tic-Tac-Toe.

## Usage

### New Project

1. Open the Lisp REPL.
2. Load the script into a Lisp REPL.
3. Play the game directly from the REPL.

```python
> (load "<path-to-project-folder>/LispTicTacToe/main.lisp")
```
The game will run through some basic test functions then immediately start.
The board is a 3 x 3 tile representation, and acts as such for inputting moves.

### Play the Game


How to start a game mode:
You will be prompted the game mode you wish to play.
Either: AI vs AI (1), AI vs User (2) or User vs User (any character).
The game will immediately start after this prompt.
```python
> Enter the type of game you wish to play: 1
> Enter the type of game you wish to play: 2
> Enter the type of game you wish to play: 3
```

How to enter a move:
```python
> x-coordincate y-coordinate
> 1 3
```