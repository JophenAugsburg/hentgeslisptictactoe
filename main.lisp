;; Print the Tic-Tac-Toe Board
(defun print-board ( board )
  (format t "=============")
  (do ((i 0 (+ i 1)))
    ((= i 9) nil)
    (if (= (mod i 3) 0)
      (format t "~%|")
      nil
    )
    (format t " ~A |" (nth i board))
  )
  (format t "~%=============")
)

;; check if 3 characters are equal
(defun threequal ( alist )
  (let ((x (first alist)) (y (nth 1 alist)) (z (nth 2 alist)))
    (if (eql x y)
      (eql y z)
      nil
    )
  )
)

;; Check if a given list of three values is a win or not (not -'s then win)
(defun three-win ( alist )
  (and (threequal alist)
    (not (eql (first alist) '- ))
  )
)

;; Grab a specific row from the board given a row number and the board
(defun grab-row ( board row )
  (let (( x (* 3 row)))
    (list (nth x board) (nth (+ x 1) board) (nth (+ x 2) board))
  )
)

;; Grab a specific column from the board given a column number and the board
(defun grab-column ( board column )
  (let (( x (- column 1)))
    (list (nth x board) (nth (+ x 3) board) (nth (+ x 6) board))
  )
)

;; Grab a specific diaginal from the board given a diaginal left or right and the board
;; left or right (0 or 1) will get the diaginal from the left-down or right-down
(defun grab-diaginal ( board leftright )
  (if (= leftright 0)
    (list (nth 0 board) (nth 4 board) (nth 8 board))
    (list (nth 2 board) (nth 4 board) (nth 6 board))
  )
)

;; go through the board rows and check for a winnng list
(defun row-win ( board )
    (or 
        (three-win(grab-row board 0)) (three-win(grab-row board 1)) (three-win(grab-row board 2))
    )
)

;; go through the board columns and check for a winnng list
(defun column-win ( board )
    (or 
        (three-win(grab-column board 1)) (three-win(grab-column board 2)) (three-win(grab-column board 3))
    )
)

;; go through the board diaginals and check for a winnng list
(defun diaginal-win ( board )
  (or (three-win(grab-diaginal board 0)) (three-win(grab-diaginal board 1)))
)

;; go through all of the win options and check if there exists a win on the board
(defun board-win ( board )
  (or (diaginal-win board) (column-win board) (row-win board))
)

;; place a move on the board
(defun place-move ( board x y value )
  (if (> (* x y) (list-length board)) ;; if entered move is within the board
    nil ;; return nil if not
    (if (or (> x 3) (< x 1)) ;; if the x position is outside board bounds
      nil
      (if (or (> y 3) (< y 1)) ;; if the y position is outside board bounds
        nil
        (if (eql (nth (+ (* (- x 1) 3) (- y 1)) board) '-) ;; if the place does holds '-'
          (setf (nth (+ (* (- x 1) 3) (- y 1)) board) value) ;; set the that value
          nil ;; return nil
        )
      )
    )
  )
)

;; count the moves in the board
(defun count-moves ( board )
  (if (null board)
    0
    (if (eql (car board) '-)
      (+ 0 (count-moves (cdr board))) ;; if equal to dash - don't count
      (+ 1 (count-moves (cdr board))) ;; otherwise count
    )
  )
)

;; convert an input string to a list
;; used for x y position on the board
(defun string-to-list (str)
    (if (not (streamp str))
        (string-to-list (make-string-input-stream str))
        (if (listen str)
            (cons (read str) (string-to-list str))
            nil
        )
    )
)

;; check if a number is between 0 and 4 - a good tic-tac-toe x or y input
(defun check123 (number)
  (and 
    (< number 4)
    (> number 0)
  )
)

;; check if a move is good. both x and y values are between 0 and 4
(defun check-move (move)
  (and
    (check123 (nth 0 move))
    (check123 (nth 1 move))
  )
)

;; calculate whether it is an x or o move
(defun x-or-o (board)
  (if (= (mod (count-moves board) 2) 0) ;; if total moves are even, enter 'x' -- otherwise enter 'o'
    'x
    'o
  )
)

;; check that a placed move is good - not out of bounds or already taken
(defun place-and-check-move (board x y)
  (place-move
    board
    x
    y
    (x-or-o board)
  )
)

;; get an input from a user.
;; make sure it is good, and recursively iterate until they enter a good input
(defun get-input (board)

  ;; get a user input
  (format t "~%Enter an x y position (ex: 1 2)")
  (if (= (mod (count-moves board) 2) 0)
    (format t " X is up: ")
    (format t " O is up: ")
  )

  (let ((move (read-line)))
    (if 
      (check-move (string-to-list move)) ;; check if the input contains a legal move
      (if 
        (null ;; place the move - if not successfully placed
          (place-and-check-move
            board
            (nth 0 (string-to-list move)) 
            (nth 1 (string-to-list move))
          )
        )
        (get-input board) ;; repeat this function to get another input
      )
      (get-input board) ;; repeat this function to get another input
    )
  )
)

;; generate 2 values for the x/y move
;; put into a length 2 list.
(defun generate-random-move ()
  (let (lst)
    (dotimes (n 2)
      (push (random 4) lst))
    (reverse lst))
)

;; have the ai pick random x and y values
;; continue until the move is allowed
(defun ai-input (board)
  (let ((move (generate-random-move))) ;; get a random move - x and y coordinate
    ;; NOTE: move is always legal - 1 - 3 x/y values
    (if 
      (null ;; place the move - if not successfully placed
        (place-and-check-move
          board
          (nth 0 move) 
          (nth 1 move)
        )
      )
      (ai-input board) ;; repeat this function to get another random input
    )
  )
)

;; handle the inputs from a user.
;; recursively iterate until the game has been completed.
(defun move-driver (board)
  ;; print the board
  (print-board board)

  (get-input board) ;; make a user move - get their input

  ;; if the game is not complete - win or tie, repeat this function for another move
  (if 
    (or 
      (eql (count-moves board) 9)
      (board-win board)
    )
    nil
    (move-driver board)
  )
)

;; handle the inputs from a user playing against an ai.
;; recursively iterate until the game has been completed.
(defun ai-user-move-driver (board)
  ;; print the board
  (print-board board)
  
  ;; make a move
  (if (= (mod (count-moves board) 2) 0) ;; check what move the game is on
    (ai-input board) ;; even move - the ai enters a move
    (get-input board) ;; odd move - the user enters
  )

  ;; if the game is not complete - win or tie, repeat this function for another move
  (if 
    (or 
      (eql (count-moves board) 9)
      (board-win board)
    )
    nil
    (ai-user-move-driver board)
  )
)

;; handle the inputs from a ai playing against an ai.
;; recursively iterate until the game has been completed.
(defun ai-move-driver (board)
  ;; print the board
  (print-board board)
  
  (ai-input board) ;; make an ai move

  ;; if the game is not complete - win or tie, repeat this function for another move
  (if 
    (or 
      (eql (count-moves board) 9)
      (board-win board)
    )
    nil
    (ai-move-driver board)
  )
)

;; from the input - start the game type
(defun start-game (board input)
  (if (string= input "1") ;; if the input is "1"
    (ai-move-driver board) ;; ai vs ai
    (if (string= input "2") ;; if not "1" is it "2"?
      (ai-user-move-driver board) ;; is "2" ai vs user
      (move-driver board) ;; default user vs user
    )
  )
)

;; main function to handle user inputs
(defun main ()
  ;; basic prompts about the game - at the beginning of the game.
  (format t "~%~%STARTING THE GAME...~%~%")
  (format t "Types of Games:~%")
  (format t "- AI vs AI (1)~%")
  (format t "- User vs AI (2)~%")
  (format t " - User vs User (3)~%")

  (format t "~%Enter the type of game you wish to play: ")
  (let (
      (answer (read-line)) ;; read the user input - get the game type they want to play
      (*board* '( - - - - - - - - -)) ;; create the board the game will be played on.
    )

    (start-game *board* answer) ;; start the game using the board and input
  
    ;; print the final board
    (print-board *board*)
    
    (if (board-win *board*) ;; if there is a win on the board
      (if (= (mod (count-moves *board*) 2) 0) ;; if total moves are even, 'o' wins -- otherwise 'x' wins
        (format t "~%O IS THE WINNER!!!")
        (format t "~%X IS THE WINNER!!!")
      )
      (format t "~%The game ended in a tie...") ;; the game is a tie - so print out there is a tie.
    )
  )
)

;; test all of the functions
;; run through functions with some base cases to make sure they all work
(defun test-program ()
    (format t "~%Testing the program...~%")
    ;; test threequal function.
    (if (and (threequal '( x x x )) (not (threequal '(x x o))))
        (format t "- threequal() works!~%")
        (format t "- threequal() does not work~%")
    )
    ;; test row-win function.
    (let 
      (
        (*row-win-brd* '( - x - o o o x - -))
        (*no-row-win-brd* '( - x - o o - x - -))
      )
      (if (and (row-win *row-win-brd*) (not (row-win *no-row-win-brd*)))
          (format t "- row-win() works!~%")
          (format t "- row-win() does not work~%")
      )
    )
    ;; test column-win function.
    (let 
      (
        (*column-win-brd* '( x x o x o - x - o))
        (*no-column-win-brd* '( - x - o o - x - -))
      )
      (if (and (column-win *column-win-brd*) (not (column-win *no-column-win-brd*)))
          (format t "- column-win() works!~%")
          (format t "- column-win() does not work~%")
      )
    )
    ;; test diaginal-win function.
    (let 
      (
        (*diaginal-win-brd* '( x x - o x - x - x))
        (*no-diaginal-win-brd* '( o x o o x - x - o))
      )
      (if (and (diaginal-win *diaginal-win-brd*) (not (diaginal-win *no-diaginal-win-brd*)))
          (format t "- diaginal-win() works!~%")
          (format t "- diaginal-win() does not work~%")
      )
    )
    ;; test the grab-row function
    (let 
      (
        (*grab-row-brd* '( - x - o o - x - -))
      )
      (if (and (equal (grab-row *grab-row-brd* 0) '(- x -)) (not (equal (grab-row *grab-row-brd* 1) '(- x -))))
          (format t "- grab-row() works!~%")
          (format t "- grab-row() does not work~%")
      )
    )
    ;; test the grab-column function
    (let 
      (
        (*grab-column-brd* '( - x - o o - x - -))
      )
      (if (and (equal (grab-column *grab-column-brd* 1) '(- o x)) (not (equal (grab-column *grab-column-brd* 2) '(- o x))))
          (format t "- grab-column() works!~%")
          (format t "- grab-column() does not work~%")
      )
    )
    ;; test the grab-diaginal function
    (let 
      (
        (*grab-diaginal-brd* '( - x - o o - x - -))
      )
      (if (and (equal (grab-diaginal *grab-diaginal-brd* 0) '(- o -)) (not (equal (grab-diaginal *grab-diaginal-brd* 1) '(- o -))))
          (format t "- grab-diaginal() works!~%")
          (format t "- grab-diaginal() does not work~%")
      )
    )
    ;; test the place-move function
    (let 
      (
        (*place-move-brd* '( - x - o o - x - -))
      )
      (place-move *place-move-brd* 2 3 'o)
      (if (equal *place-move-brd* '( - x - o o o x - -))
          (format t "- place-move() works!~%")
          (format t "- place-move() does not work~%")
      )
    )
  )

(test-program) ;; run through basic tests for the game
(main) ;; start the game